Content of this folder:
- aws_setup.txt	file describing the steps to perform on AWS F1 instance
- example.txt	example of input file to SDAccel host binary
- main.cpp		SDAccel host file
- Makefile		Makefile to build the project
- src/			folder containing the kernel to be synthesized with SDAccel


aws_setup.txt:
This file contains the steps necessary to run the proposed design on AWS F1 instance.
Before launching an F1 instance, please remember to create a S3 bucket and upload the bitstream.
The bitstream of our design is available here: https://www.dropbox.com/sh/0wggmxtmuumzz4w/AABDOLAIDZqKUQJCY7eE5AOha?dl=0
Please, refer to https://github.com/aws/aws-fpga in order to create the Amazon FPGA Image (AFI) and the awsxclbin file


example.txt:
An example of input file. Such file should be organized as follows:
NUMBER OF COUPLES
Couple 0: 5 points of view 0 (0_view_0_point_0.x, 0_view_0_point_0.y, 0_view_0_point_1.x, 0_view_0_point_1.y, ...)
Couple 0: 5 points of view 1 (0_view_1_point_0.x, 0_view_1_point_0.y, 0_view_1_point_1.x, 0_view_1_point_1.y, ...)
Couple 1: 5 points of view 0 (1_view_0_point_0.x, 1_view_0_point_0.y, 1_view_0_point_1.x, 1_view_0_point_1.y, ...)
Couple 1: 5 points of view 1 (1_view_1_point_0.x, 1_view_1_point_0.y, 1_view_1_point_1.x, 1_view_1_point_1.y, ...)
...


main.cpp:
Please, run the host with the following parameters:
./host file.xclbin inputData.txt

The file.xclbin is mandatory, while the inputData.txt file (for instance, example.txt file) is optional.
If only the file.xclbin is provided, the host runs in demo mode, i.e. it invokes the kernel on FPGA on an increasing number of couples of 5 points
If the user provides also an input data file, the host invokes the kernel on FPGA on the provided points, and prints the essential matrices, along with the number of errors below a certain treshold (defined within the host code).


Makefile:
To build the project, please use this command (after sourcing SDx 2017.1.op settings65.sh script):
make build TARGET=hw

The output of the build is located in folder: hw/xilinx_aws-vu9p-f1_4ddr-xpr-2pr_4.0/
Please, refer to the Makefile in order to change parameters like the target board (Xilinx Virtex Ultrascale+ VU9P FPGA) or target frequency (170 MHz)
Here the Makefile usage:
make [emulation | build | clean | clean_sw_emu | clean_hw_emu | clean_hw | cleanall] TARGET=<sw_emu | hw_emu | hw>


src/:
This folder contains the kernel files.
kernel.cpp file contains the top function (namely, kernel), while the other files contain support functions
This folder also contains testbench.cpp file, i.e. the testbench for Vivado HLS.

