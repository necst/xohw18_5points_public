#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include <assert.h>
#include <stdbool.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <CL/opencl.h>
#include <CL/cl.h>
#include <cstdlib>
#include <ctime>
#include <fstream>

#define TOTAL_ITER 100 //number of iterations
#define NUM_POINTS 5 //number of points
#define NUM_OUT 10 //number of output points
#define INPUT_ITER TOTAL_ITER * NUM_POINTS * 2 //total input per run
#define OUTPUT_ITER 10 * TOTAL_ITER * NUM_OUT //total output per run

typedef float point;
typedef float mytype;

//threshold
float thresh = 0.2f;
  

/*
	Given an event, this function returns the kernel execution time in ms
*/
float getTimeDifference(cl_event event) {
	cl_ulong time_start = 0;
	cl_ulong time_end = 0;
	float total_time = 0.0f;

	clGetEventProfilingInfo(event,
		CL_PROFILING_COMMAND_START,
		sizeof(time_start),
		&time_start,
		NULL);
	clGetEventProfilingInfo(event, 
		CL_PROFILING_COMMAND_END,
		sizeof(time_end),
		&time_end,
		NULL);
	total_time = time_end - time_start;
	return total_time/ 1000000.0; // To convert nanoseconds to milliseconds
}
    /* 
    return a random number between 0 and limit inclusive.
    */
    int rand_lim(int limit) {
      
      int divisor = RAND_MAX/(limit+1);
      int retval;

      do { 
        retval = rand() / divisor;
      } while (retval > limit);

      return retval;
    }

    /*
    Fill the string with random values
    */
    void fillRandom(unsigned char* string, int dimension){
        //fill the string with random letters.. 
      static const char possibleLetters[] = "ATCG";

      string[0] = '-';

      int i;
      for(i = 0; i<dimension; i++){
        int randomNum = rand_lim(3);
        string[i] = possibleLetters[randomNum];
      }



    }
int
load_file_to_memory(const char *filename, char **result)
{ 
  int size = 0;
  FILE *f = fopen(filename, "rb");
  if (f == NULL) 
  { 
    *result = NULL;
    return -1; // -1 means file opening fail 
  } 
  fseek(f, 0, SEEK_END);
  size = ftell(f);
  fseek(f, 0, SEEK_SET);
  *result = (char *)malloc(size+1);
  if (size != fread(*result, sizeof(char), size, f)) 
  { 
    free(*result);
    return -2; // -2 means file reading fail 
  } 
  fclose(f);
  (*result)[size] = 0;
  return size;
}

/* Read points from input file*/
void readValuesFromFile(std::ifstream &myfile, point *pt, int index){

  std::string line1;
  int i = 0;
  getline (myfile,line1);
  std::size_t start = 0;
  std::size_t found = line1.find_first_of('\t');

  while (found!=std::string::npos){
    std::string val = line1.substr(start, found-start);
    pt[index + i] = atof(val.c_str());
    i++;
    start = found+1;
    found=line1.find_first_of('\t',found+1);
  }

  std::string val = line1.substr(start, found-start);
  pt[index + i] = atof(val.c_str());

}

int main(int argc, char** argv)
{

	srand (static_cast <unsigned> (time(0)));

	printf("HOST Started \n");
	fflush(stdout);
  	int err;                            // error code returned from api calls

	unsigned int real_iters;
	unsigned int start_iter;
	unsigned int real_input_points;
	unsigned int real_output_points;
	bool print_output = false;

	point * input_a; 
	point * input_b;
	mytype *output;


	if(argc == 1){
		printf("Please, provide the xclbin file and, if you want to evaluate \"5 points to rule the all\" design with your own data, provide a tab delimited file organized as follows:\n");
                printf("NUMBER OF COUPLES\n");
                printf("Couple 0: 5 points of view 0\n");
                printf("Couple 0: 5 points of view 1\n");
                printf("Couple 1: 5 points of view 0\n");
                printf("Couple 1: 5 points of view 1\n");
                printf("...\n");
		return -1;
	}else if(argc == 2){
		printf("Demo version\nThe host code will randomly generate %d couples of 5 points\n", TOTAL_ITER);
		printf("The host will invoke the FPGA on an increasing number of couples ranging from 1 to %d\n", TOTAL_ITER);
		printf("If you want to evaluate \"5 points to rule the all\" design with your own data, provide a tab delimited file organized as follows:\n");
		printf("NUMBER OF COUPLES\n");
		printf("Couple 0: 5 points of view 0\n");
		printf("Couple 0: 5 points of view 1\n");
		printf("Couple 1: 5 points of view 0\n");
		printf("Couple 1: 5 points of view 1\n");
		printf("...\n");
		real_iters = TOTAL_ITER;
		start_iter = 1;
		real_input_points = INPUT_ITER;
		real_output_points = OUTPUT_ITER;

		input_a = (point *)malloc(sizeof(point) * INPUT_ITER);
		input_b = (point *)malloc(sizeof(point) * INPUT_ITER);
  		output = (mytype *)malloc(sizeof(point) * OUTPUT_ITER);		

		//input initialization
		for(int i = 0; i < INPUT_ITER; i+=2){
			input_a[i] = static_cast <float> (i/2 + 1);
		      	input_a[i+1] = static_cast <float> ((i/2 + 1)*2);
		        input_b[i] = static_cast <float> ((i/2 + 1)*3) ;
		        input_b[i+1] = static_cast <float> ((i/2 + 1)*4) ;
		}
		
		
		for(int i = 0; i < OUTPUT_ITER; i++){	
			output[i] = 0.0;
		}               

	} else {
		std::ifstream myfile (argv[2]);
		if (myfile.is_open()){
			printf("Reading input from file %s\n", argv[2]);
			std::string line1;
      			getline (myfile,line1);
      			real_iters = atoi(line1.c_str());
			start_iter = real_iters;
			real_input_points = real_iters * NUM_POINTS * 2;
			real_output_points = real_iters * NUM_OUT * 10;
			print_output = true;
			input_a = (point *)malloc(sizeof(point) * real_input_points);
                	input_b = (point *)malloc(sizeof(point) * real_input_points);
                	output = (mytype *)malloc(sizeof(point) * real_output_points);
			int index = 0;
			while(!myfile.eof()){
      				readValuesFromFile(myfile, input_a, index);
      				readValuesFromFile(myfile, input_b, index);
				index += 10;
    			}
    			myfile.close();
  		} else {
			printf("Unable to open file %s\n", argv[2]);
			return -1;
		}
	}


//	printf("define 2x %d elements in Input, %d elements in output expected \n", INPUT_ITER, OUTPUT_ITER);
	
	fflush(stdout);

  cl_platform_id platform_id;         // platform id
  cl_device_id device_id;             // compute device id 
  cl_context context;                 // compute context
  cl_command_queue commands;          // compute command queue
  cl_program program;                 // compute program
  cl_kernel kernel;                   // compute kernel
   
  char cl_platform_vendor[1001];
  char cl_platform_name[1001];
   
  cl_mem input_a_buff;                     // device memory used for the input array
  cl_mem input_b_buff;                     // device memory used for the input array
  cl_mem out_buff;			   // device memory used for the output array

   

  // Fill our data sets with pattern
  //
  int i = 0;
  
  fflush(stdout);
  
  // Connect to first platform
  //
	printf("GET platform \n");
  err = clGetPlatformIDs(1,&platform_id,NULL);
  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to find an OpenCL platform!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
	printf("GET platform vendor \n");
  err = clGetPlatformInfo(platform_id,CL_PLATFORM_VENDOR,1000,(void *)cl_platform_vendor,NULL);
  if (err != CL_SUCCESS)
  {
    printf("Error: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
  printf("CL_PLATFORM_VENDOR %s\n",cl_platform_vendor);
	printf("GET platform name \n");
  err = clGetPlatformInfo(platform_id,CL_PLATFORM_NAME,1000,(void *)cl_platform_name,NULL);
  if (err != CL_SUCCESS)
  {
    printf("Error: clGetPlatformInfo(CL_PLATFORM_NAME) failed!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
  printf("CL_PLATFORM_NAME %s\n",cl_platform_name);
 
  // Connect to a compute device
  //
  int fpga = 0;
#if defined (FPGA_DEVICE)
  fpga = 1;
#endif
	printf("get device, fpga is %d \n", fpga);
  err = clGetDeviceIDs(platform_id, fpga ? CL_DEVICE_TYPE_ACCELERATOR : CL_DEVICE_TYPE_CPU,
                       1, &device_id, NULL);
  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to create a device group!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
  
  // Create a compute context 
  //
	printf("create context \n");
  context = clCreateContext(0, 1, &device_id, NULL, NULL, &err);
  if (!context)
  {
    printf("Error: Failed to create a compute context!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  // Create a command commands
  //
	printf("create queue \n");
  commands = clCreateCommandQueue(context, device_id, CL_QUEUE_PROFILING_ENABLE, &err);
  if (!commands)
  {
    printf("Error: Failed to create a command commands!\n");
    printf("Error: code %i\n",err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  int status;

  // Create Program Objects
  //
  
  // Load binary from disk
  unsigned char *kernelbinary;
  char *xclbin=argv[1];
  printf("loading %s\n", xclbin);
  int n_i = load_file_to_memory(xclbin, (char **) &kernelbinary);
  if (n_i < 0) {
    printf("failed to load kernel from xclbin: %s\n", xclbin);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
  size_t n = n_i;
  // Create the compute program from offline
	printf("create program with binary \n");
  program = clCreateProgramWithBinary(context, 1, &device_id, &n,
                                      (const unsigned char **) &kernelbinary, &status, &err);
  if ((!program) || (err!=CL_SUCCESS)) {
    printf("Error: Failed to create compute program from binary %d!\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  // Build the program executable
  //
	printf("build program \n");
  err = clBuildProgram(program, 0, NULL, NULL, NULL, NULL);
  if (err != CL_SUCCESS)
  {
    size_t len;
    char buffer[2048];

    printf("Error: Failed to build program executable!\n");
    clGetProgramBuildInfo(program, device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
    printf("%s\n", buffer);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  // Create the compute kernel in the program we wish to run
  //
	printf("create kernel \n");
  kernel = clCreateKernel(program, "kernel", &err);
  if (!kernel || err != CL_SUCCESS)
  {
    printf("Error: Failed to create compute kernel!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

//In case the kernel exploits multiple memory banks
/*
  cl_mem_ext_ptr_t input_a_ext;
  cl_mem_ext_ptr_t input_b_ext;
  cl_mem_ext_ptr_t out_buff_ext;

  input_a_ext.flags = XCL_MEM_DDR_BANK0;
  input_a_ext.obj = 0;
  input_a_ext.param = 0;

  input_b_ext.flags = XCL_MEM_DDR_BANK1;
  input_b_ext.obj = 0;
  input_b_ext.param = 0;


  out_buff_ext.flags = XCL_MEM_DDR_BANK0; 
  out_buff_ext.obj = 0;
  out_buff_ext.param = 0;
*/

//buffer creations
	printf("create buffer 0 \n");
  input_a_buff = clCreateBuffer(context,  CL_MEM_READ_WRITE,  sizeof(point) * real_input_points, NULL, NULL);
if (!input_a) 
  {
    printf("Error: Failed to allocate device memory!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  } 

  printf("create buffer 1 \n");
  input_b_buff = clCreateBuffer(context,  CL_MEM_READ_WRITE,  sizeof(point) * real_input_points, NULL, NULL);
if (!input_b) 
  {
    printf("Error: Failed to allocate device memory!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }    

  printf("create buffer 2 \n");
  out_buff = clCreateBuffer(context,  CL_MEM_READ_WRITE,  sizeof(mytype) * real_output_points, NULL, NULL);
if (!input_b) 
  {
    printf("Error: Failed to allocate device memory!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  } 


//Writing on buffers
  err = 0;
  printf("writing buffer a \n");
  err = clEnqueueWriteBuffer(commands, input_a_buff, CL_TRUE, 0, sizeof(point) * real_input_points, input_a, 0, NULL, NULL);

  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to write to source array a!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  printf("writing buffer b \n");
  err = clEnqueueWriteBuffer(commands, input_b_buff, CL_TRUE, 0, sizeof(point) * real_input_points, input_b, 0, NULL, NULL);

  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to write to source array a!\n");
    printf("Test failed\n");
    return EXIT_FAILURE;
  }


for(int iterations=start_iter; iterations <= real_iters; iterations++){
    
  // Set the arguments to our compute kernel
  
  err = 0;
  
  err  = clSetKernelArg(kernel, 0, sizeof(cl_mem), &input_a_buff);
  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to set kernel arguments 0! %d\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
  
  err  = clSetKernelArg(kernel, 1, sizeof(cl_mem), &input_b_buff);
  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to set kernel arguments 1! %d\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
  
  err  = clSetKernelArg(kernel, 2, sizeof(cl_mem), &out_buff);
  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to set kernel arguments 2! %d\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
  
  err  = clSetKernelArg(kernel, 3, sizeof(int), &iterations);
  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to set kernel arguments 3! %d\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  err  = clSetKernelArg(kernel, 4, sizeof(float), &thresh);
  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to set kernel arguments 4! %d\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

//start kernel
cl_event enqueue_kernel;
  err = clEnqueueTask(commands, kernel, 0, NULL, &enqueue_kernel);
  if (err)
  {
    printf("Error: Failed to execute kernel! %d\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }
	fflush(stdout);
//wait for the end of computation
  clWaitForEvents(1, &enqueue_kernel);
	fflush(stdout);

  // Read back the results from the device to verify the output
  //
  cl_event readevent;
  err = clEnqueueReadBuffer( commands, out_buff, CL_TRUE, 0, sizeof(mytype) * real_output_points, output, 0, NULL, &readevent );  
  if (err != CL_SUCCESS)
  {
    printf("Error: Failed to read output array! %d\n", err);
    printf("Test failed\n");
    return EXIT_FAILURE;
  }

  clWaitForEvents(1, &readevent);

//print output
if(print_output){
  printf("\n");
  for(i = 0; i < OUTPUT_ITER; i++){
    if(i % NUM_OUT == 0)
      printf("\n");
    printf(" %f ", output[i]);

 }
 printf("\n\n");
}


 float executionTime = getTimeDifference(enqueue_kernel); 	    
 printf("couples: %d\ttime[ms]: %f\tavg time[ms]: %f\n", iterations, executionTime, executionTime / iterations);
    
}

  // Print a brief summary detailing the results  
  // Shutdown and cleanup
  
  clReleaseMemObject(input_a_buff);
  clReleaseMemObject(input_b_buff);
  clReleaseMemObject(out_buff);
  clReleaseProgram(program);
  clReleaseKernel(kernel);
  clReleaseCommandQueue(commands);
  clReleaseContext(context);

  free(input_a);
  free(input_b);
  free(output);

  return EXIT_SUCCESS;
}
