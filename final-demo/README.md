# XOHW final demo

This folder contains the final demo for the Xilinx Open Hardware Design Content. It is a client-server application: 

- The **server** receives as input two sets of matching points coming from two corresponding images, applies the 5-point Nister algorithm within the RANSAC framework and, finally, returns the rotation and translation matrix between the two images seen by the camera. The server can either perform the computation using the CPU or the FPGA as required by the client.

- The **client** is a web-based application that sends to the server the matching points from two subsequent images and wait for the response in order to reconstruct the trajectory of the moving camera. The images seen by the camera as well as the reconstructed trajectory are plotted to the screen.

The images and the matching points between images are taken from the [KITTI dataset](http://www.cvlibs.net/datasets/kitti/eval_odometry.php). In particular, we use the sequence 00 and the images taken from camera 0.

# How to run the demo

## Running the server

The server can either be executed locally (support for CPU execution only) or on a AWS F1 instance (support for both FPGA and CPU execution). In order to run and install the server follow the instructions within the [server folder](server/)

## Running the client

The client is a simple static HTML/javascript application. In order to run the client you only need a web browser (we suggest using the latest version of chrome to avoid compatibility issues). However, before running the client, you should also download the images of sequence 00, camera 0, from the KITTI dataset. You can download the data from the following link:

[http://www.cvlibs.net/download.php?file=data_odometry_gray.zip](http://www.cvlibs.net/download.php?file=data_odometry_gray.zip)

place the images within the folder: *client/KITTI-images-seq-00*
The folder already contains the first 10 images of sequence 00. You should include all the remaining images within the folder.

Then, to run the client simply open **index.html**, insert the address and port of the server within the web page, then click *connect*. Once the client is connected to the server you can then specify the number of runsac iteration from the dropdown menu (higher number means higher precision at the cost of higher execution time) and then either click on *Run 5-point algorithm on CPU* or on *Run 5-point algorithm on FPGA* (available only if the server is running on a AWS F1 instance and has been compiled with FPGA support).

	
