#ifndef AWS_DEMO_NISTER_FPGA_H
#define AWS_DEMO_NISTER_FPGA_H

#include "../global.h"
#include <opengv/types.hpp>
#include <fstream>
#ifdef AWS_DEPLOY
#include <CL/opencl.h>
#include <CL/cl.h>
#else
#include <OpenCL/opencl.h>
#include <OpenCL/cl.h>
#endif




typedef struct {

    cl_platform_id platform_id;         // platform id
    cl_device_id device_id;             // compute device id
    cl_context context;                 // compute context
    cl_command_queue commands;          // compute command queue
    cl_program program;                 // compute program
    cl_kernel kernel;                   // compute kernel

    char cl_platform_vendor[1001];
    char cl_platform_name[1001];

    cl_mem input_a_buff;                     // device memory used for the input array
    cl_mem input_b_buff;                     // device memory used for the input array
    cl_mem out_buff;			   // device memory used for the output array

} kernel_context;

void init_fpga_kernel(kernel_context & context, char *xclbin, std::ofstream & log);
void multi_nister_fpga(point *p1, point *p2, int num_iter,
                       opengv::essentials_t & essentials, essential_sizes_t & essential_sizes,
                       kernel_context & context, std::ofstream &log);
void release_fpga_kernel(kernel_context & context);

#endif // AWS_DEMO_NISTER_FPGA_H