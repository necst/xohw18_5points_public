#ifndef AWS_DEMO_RANSAC_H
#define AWS_DEMO_RANSAC_H

#include "../global.h"
#include <unistd.h>
#include <iostream>
#include <opengv/types.hpp>

void gen_5_point_problems(point *p1_in, point *p2_in, size_t n_points,
                          point *p1_out, point *p2_out, size_t n_problems, size_t * out_indices,
                          int *rand_numbers, size_t num_rand_numbers, size_t &rand_offset, std::ofstream & log);

bool recover_best_transformation(opengv::essentials_t & essentials, essential_sizes_t & essential_sizes,
                                 size_t * indices, point *p1, point *p2, size_t num_points,
                                 opengv::transformation_t & outModel, double ransac_threshold, double & quality);

bool ransac(opengv::essentials_t & essentials, essential_sizes_t & essential_sizes,
            size_t * indices, point *p1, point *p2, size_t num_points,
            opengv::transformation_t & outModel, double ransac_threshold, double & best_quality);

#endif //AWS_DEMO_RANSAC_H
