#ifndef AWS_DEMO_UTILS_H
#define AWS_DEMO_UTILS_H

#include <sys/time.h>
#include <unistd.h>

double timediff( const struct timeval &t1, const struct timeval &t2 );
void init_random_numbers(int seed, size_t num_values, int *numbers);

#endif //AWS_DEMO_UTILS_H
