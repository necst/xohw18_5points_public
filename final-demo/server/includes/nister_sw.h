#ifndef AWS_DEMO_NISTER_SW_H
#define AWS_DEMO_NISTER_SW_H

#include "../global.h"
#include <opengv/types.hpp>
#include <opengv/relative_pose/CentralRelativeAdapter.hpp>

void my_fivept_nister(const opengv::relative_pose::RelativeAdapterBase & adapter, opengv::essentials_t & essentials);

void multi_nister_sw(point *p1, point *p2, size_t num_iter, opengv::essentials_t & essentials,
                     essential_sizes_t & essential_sizes);

#endif //AWS_DEMO_NISTER_SW_H
