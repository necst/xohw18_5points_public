#include "global.h"
#include "includes/nister_sw.h"
#include "includes/utils.h"
#include "includes/ransac.h"
#include "includes/nister_fpga.h"
#include <iostream>
#include <fstream>
#include <iomanip>
#include <opengv/relative_pose/methods.hpp>

using namespace std;
using namespace Eigen;
using namespace opengv;

int main( int argc, char** argv )
{
    // variables to get timing info
    struct timeval tic;
    struct timeval toc;

    // log stream to file
    ofstream log;
    log.open("log.txt", ios::trunc);

    point p1[MAX_INPUT_POINTS];
    point p2[MAX_INPUT_POINTS];
    point p1_orig[MAX_INPUT_POINTS];
    point p2_orig[MAX_INPUT_POINTS];
    point p1_ransac[MAX_RANSAC_ITER * POINTS_PER_ITER];
    point p2_ransac[MAX_RANSAC_ITER * POINTS_PER_ITER];

    size_t indices[MAX_RANSAC_ITER * TOTAL_POINTS_PER_ITER];

    int random_numbers[NUM_RANDOM_NUMBERS];
    init_random_numbers(0, NUM_RANDOM_NUMBERS, random_numbers);
    size_t n_points, message_id, num_ransac_iter, rand_offset = 0;
    double ransac_threshold;

    five_point_algorithm algorithm;
    int algorithm_num;

    bearingVectors_t b1(POINTS_PER_ITER), b2(POINTS_PER_ITER);
    relative_pose::CentralRelativeAdapter relAdapter(b1, b2);
    essentials_t essentials;
    essential_sizes_t essential_sizes;
    transformation_t transformation;

    kernel_context fpga_nister_context;
    bool fpga_kernel_initialized = false;

    if(argc >= 2) {
        // try to initialize the FPGA context
        log << "Trying to initialize FPGA context using program file: " << argv[1] << endl;

        init_fpga_kernel(fpga_nister_context, argv[1], log);
        fpga_kernel_initialized = true;
    }

    cout << "READY" << endl;

    while(1) {

        // reset data structures
        essentials.clear();
        essential_sizes.clear();

        // #### 1) read data from stdin

        // read message id
        cin >> message_id;

        // read number of input points
        cin >> n_points;

        log << "number of points: " << n_points << endl;

        if(n_points > MAX_INPUT_POINTS) {
            log << "ERROR: number of input points (" << n_points << " ) exceeds " << MAX_INPUT_POINTS << endl;
            return 1;
        }

        if(n_points == 0 || cin.eof()) {
            log << "Terminating..." << endl;
            break;
        }

        // read method for solving the 5-point problem
        cin >> algorithm_num;

        log << "algorithm: " << algorithm_num << endl;

        if(algorithm_num != five_point_algorithm::nister_fpga &&
           algorithm_num != five_point_algorithm::nister_sw) {
            log << "ERROR: invalid five point algorithm" << endl;
            return 1;
        }
        algorithm = (five_point_algorithm) algorithm_num;

        // read number of RANSAC iterations
        cin >> num_ransac_iter;

        // read ransac_threshold
        cin >> ransac_threshold;

        log << "ransac iterations: " << num_ransac_iter << endl;

        if(num_ransac_iter > MAX_RANSAC_ITER) {
            log << "ERROR: number of ransac iterations (" << num_ransac_iter << " ) exceeds " << MAX_RANSAC_ITER << endl;
            return 1;
        }

        // read input points data
        for(int i = 0; i < n_points; i++) {
            cin >> p1[i].x >> p1[i].y;
            p1_orig[i].x = p1[i].x;
            p1_orig[i].y = p1[i].y;
        }
        for(int i = 0; i < n_points; i++) {
            cin >> p2[i].x >> p2[i].y;
            p2_orig[i].x = p2[i].x;
            p2_orig[i].y = p2[i].y;
        }

        double generation_time, essentials_time, recovery_time;

        // #### 2) generate 5-point problems to solve
        gettimeofday( &tic, 0 );
        gen_5_point_problems(p1, p2, n_points, p1_ransac, p2_ransac, num_ransac_iter, indices,
                          random_numbers, NUM_RANDOM_NUMBERS, rand_offset, log);
        gettimeofday( &toc, 0 );
        generation_time = timediff(toc, tic);

        // #### 3) solve 5-point problem using the selected method

        // reserve memory in advance to speed up things
        essentials.reserve(num_ransac_iter * 10);
        essential_sizes.reserve(num_ransac_iter);

        // retrieve the essential matrices
        gettimeofday( &tic, 0 );
        if (algorithm == five_point_algorithm::nister_sw) {
            multi_nister_sw(p1_ransac, p2_ransac, num_ransac_iter, essentials, essential_sizes);
        }
        if(algorithm == five_point_algorithm::nister_fpga) {
            multi_nister_fpga(p1_ransac, p2_ransac, num_ransac_iter, essentials, essential_sizes, fpga_nister_context, log);
        }
        gettimeofday( &toc, 0 );
        essentials_time = timediff(toc, tic);

        // estimate the best transformation
        double quality;
        gettimeofday( &tic, 0 );
        ransac(essentials, essential_sizes, indices, p1_orig, p2_orig, n_points, transformation, ransac_threshold, quality);
        //recover_best_transformation(essentials, essential_sizes, indices, p1_orig, p2_orig, n_points, transformation, ransac_threshold, quality);
        gettimeofday( &toc, 0 );
        recovery_time = timediff(toc, tic);

        // send result to output
        // message id
        cout << message_id << " ";

        // the first 9 coefficients represent the linearized rotation matrix
        for(int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                cout << setprecision(16) << transformation(i, j) << " ";
            }
        }


        // the following 3 coefficients are the translation vector
        for(int i = 0; i < 3; i++) {
            cout << setprecision(16) << transformation(i, 3) << " ";
        }

        // the final coefficients related to timing information
        cout << setprecision(16) << recovery_time << " " << essentials_time << " " << generation_time << " " << quality;
        cout << endl;
        cout.flush();

        log << "best transformation recovery time: " << recovery_time << endl;
        log << "essentials computation exec time: " << essentials_time << endl;
        log << "RANSAC generation exec time: " << generation_time << endl;

        /*
        cout << "solution:" << endl;
        for (opengv::essential_t e: essentials) {
            cout << e << endl;
        }
         */
    }

    if(fpga_kernel_initialized) {
        // release FPGA context
        release_fpga_kernel(fpga_nister_context);
    }

    return 0;
}
