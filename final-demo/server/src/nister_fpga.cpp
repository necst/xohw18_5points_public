#include "../includes/nister_fpga.h"
#include "../global.h"
#include <opengv/types.hpp>

#include <iostream>
#include <fstream>

using namespace std;

#define NISTER_FPGA_MAX_OUT_SIZE (MAX_RANSAC_ITER * 10 * 10)
#define NISTER_FPGA_MAX_IN_SIZE (MAX_RANSAC_ITER * POINTS_PER_ITER)

float __fpga_output_buffer[NISTER_FPGA_MAX_OUT_SIZE];

int load_file_to_memory(const char *filename, char **result)
{
    int size = 0;
    FILE *f = fopen(filename, "rb");
    if (f == NULL)
    {
        *result = NULL;
        return -1; // -1 means file opening fail
    }
    fseek(f, 0, SEEK_END);
    size = ftell(f);
    fseek(f, 0, SEEK_SET);
    *result = (char *)malloc(size+1);
    if (size != fread(*result, sizeof(char), size, f))
    {
        free(*result);
        return -2; // -2 means file reading fail
    }
    fclose(f);
    (*result)[size] = 0;
    return size;
}


void init_fpga_kernel(kernel_context & context, char *xclbin, ofstream & log)
{
    int err; // error code returned from api calls

    // get platform
    log << "OpenCL: Get platform IDs" << endl;
    err = clGetPlatformIDs(1, &context.platform_id, NULL);
    if (err != CL_SUCCESS) {
        log << "Error: Failed to find an OpenCL platform!" << endl;
        log.flush();
        exit(-1);
    }

    // get platform vendor
    log << "OpenCL: Get platform vendor" << endl;
    err = clGetPlatformInfo(context.platform_id, CL_PLATFORM_VENDOR, 1000, (void *) context.cl_platform_vendor, NULL);
    if (err != CL_SUCCESS) {
        log << "Error: clGetPlatformInfo(CL_PLATFORM_VENDOR) failed!" << endl;
        log.flush();
        exit(-1);
    }
    log << "CL_PLATFORM_VENDOR: " << context.cl_platform_vendor << endl;

    // get platform name
    log << "OpenCL: Get platform name" << endl;
    err = clGetPlatformInfo(context.platform_id, CL_PLATFORM_NAME, 1000, (void *) context.cl_platform_name, NULL);
    if (err != CL_SUCCESS) {
        log << "Error: clGetPlatformInfo(CL_PLATFORM_NAME) failed!" << endl;
        log.flush();
        exit(-1);
    }
    log << "CL_PLATFORM_NAME " << context.cl_platform_name << endl;

    // Connect to a compute device
    //
#ifdef AWS_DEPLOY
    int device_type = CL_DEVICE_TYPE_ACCELERATOR;
    log << "FPGA Device type" << endl;
#else
    int device_type = CL_DEVICE_TYPE_CPU;
    log << "CPU Device type" << endl;
#endif

    log << "OpenCL: Get device" << endl;
    err = clGetDeviceIDs(context.platform_id, device_type, 1, &context.device_id, NULL);
    if (err != CL_SUCCESS) {
        log << "Error: Failed to create a device group!" << endl;
        log.flush();
        exit(-1);
    }

    // Create a compute context
    //
    log << "OpenCL: Create context" << endl;
    context.context = clCreateContext(0, 1, &context.device_id, NULL, NULL, &err);
    if (!context.context) {
        log << "Error: Failed to create a compute context!" << endl;
        log.flush();
        exit(-1);
    }

    // Create a command commands
    //
    log << "OpenCL: Create queue" << endl;
    context.commands = clCreateCommandQueue(context.context, context.device_id, CL_QUEUE_PROFILING_ENABLE, &err);
    if (!context.commands) {
        log << "Error: Failed to create a command commands!" << endl;
        log << "Error: code " << err << endl;
        log.flush();
        exit(-1);
    }

    // Create Program Objects
    //

    // Load binary from disk
    unsigned char *kernelbinary;

    log << "OpenCL: Loading binary from disk: " << xclbin << endl;
    int n_i = load_file_to_memory(xclbin, (char **) &kernelbinary);
    if (n_i < 0) {
        log << "Failed to load kernel from xclbin: " << xclbin;
        log.flush();
        exit(-1);
    }
    size_t n = n_i;

    // Create the compute program from offline
    int status;
    log <<  "OpenCL: Create program with binary" << endl;
    context.program = clCreateProgramWithBinary(context.context, 1, &context.device_id, &n,
                                                (const unsigned char **) &kernelbinary, &status, &err);
    if ((!context.program) || (err != CL_SUCCESS)) {
        log << "Error: Failed to create compute program from binary, error: " << err << endl;
        log.flush();
        exit(-1);
    }

    // Build the program executable
    //
    log << "OpenCL: Build program " << endl;
    err = clBuildProgram(context.program, 0, NULL, NULL, NULL, NULL);
    if (err != CL_SUCCESS) {
        size_t len;
        char buffer[2048];
        log << "Error: Failed to build program executable!" << endl;
        clGetProgramBuildInfo(context.program, context.device_id, CL_PROGRAM_BUILD_LOG, sizeof(buffer), buffer, &len);
        log << buffer << endl;
        log.flush();
        exit(-1);
    }

    // Create the compute kernel in the program we wish to run
    //
    log << "OpenCL: Create kernel" << endl;
    context.kernel = clCreateKernel(context.program, "kernel", &err);
    if (!context.kernel || err != CL_SUCCESS) {
        log << "Error: Failed to create compute kernel!" << endl;
        log.flush();
        exit(-1);
    }

    //buffer creations
    log << "OpenCL: Create buffer input_a" << endl;
    context.input_a_buff = clCreateBuffer(context.context, CL_MEM_READ_WRITE, sizeof(point) * NISTER_FPGA_MAX_IN_SIZE,
                                          NULL, NULL);
    if (!context.input_a_buff) {
        log << "Error: Failed to allocate device memory for input_a!" << endl;
        log.flush();
        exit(-1);
    }

    log << "OpenCL: Create buffer input_b" << endl;
    context.input_b_buff = clCreateBuffer(context.context, CL_MEM_READ_WRITE, sizeof(point) * NISTER_FPGA_MAX_IN_SIZE,
                                          NULL, NULL);
    if (!context.input_b_buff) {
        log << "Error: Failed to allocate device memory for input_b!" << endl;
        log.flush();
        exit(-1);
    }

    log << "OpenCL: Create buffer out_bugg" << endl;
    context.out_buff = clCreateBuffer(context.context, CL_MEM_READ_WRITE, NISTER_FPGA_MAX_OUT_SIZE, NULL, NULL);
    if (!context.out_buff) {
        log << "Error: Failed to allocate device memory for out_buff!" << endl;
        log.flush();
        exit(-1);
    }
}

void multi_nister_fpga(point *p1, point *p2, int num_iter,
                       opengv::essentials_t & essentials, essential_sizes_t & essential_sizes,
                       kernel_context & context, ofstream &log)
{
    float thresh = 1e-2;

    // Write input buffers
    int err = 0;
    log << "writing buffer a" << endl;
    err = clEnqueueWriteBuffer(context.commands, context.input_a_buff, CL_TRUE, 0,
                               sizeof(point) * num_iter * POINTS_PER_ITER, p1, 0, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        log << "Error: Failed to write to source array a!" << endl;
        log.flush();
        exit(-1);
    }

    log << "writing buffer b" << endl;
    err = clEnqueueWriteBuffer(context.commands, context.input_b_buff, CL_TRUE, 0,
                               sizeof(point) * num_iter * POINTS_PER_ITER, p2, 0, NULL, NULL);
    if (err != CL_SUCCESS)
    {
        log << "Error: Failed to write to source array b!" << endl;
        log.flush();
        exit(-1);
    }

    // Set the arguments to 5-point compute kernel
    err  = clSetKernelArg(context.kernel, 0, sizeof(cl_mem), &context.input_a_buff);
    if (err != CL_SUCCESS)
    {
        log << "Error: Failed to set kernel arguments 0! " << err << endl;
        log.flush();
        exit(-1);
    }

    err  = clSetKernelArg(context.kernel, 1, sizeof(cl_mem), &context.input_b_buff);
    if (err != CL_SUCCESS)
    {
        log << "Error: Failed to set kernel arguments 1! " << err << endl;
        log.flush();
        exit(-1);
    }

    err  = clSetKernelArg(context.kernel, 2, sizeof(cl_mem), &context.out_buff);
    if (err != CL_SUCCESS)
    {
        log << "Error: Failed to set kernel arguments 2! " << err << endl;
        log.flush();
        exit(-1);
    }

    err  = clSetKernelArg(context.kernel, 3, sizeof(int), &num_iter);
    if (err != CL_SUCCESS)
    {
        log << "Error: Failed to set kernel arguments 3! " << err << endl;
        log.flush();
        exit(-1);
    }

    err  = clSetKernelArg(context.kernel, 4, sizeof(float), &thresh);
    if (err != CL_SUCCESS)
    {
        log << "Error: Failed to set kernel arguments 4! " << err << endl;
        log.flush();
        exit(-1);
    }

    // Start kernel
    log << "Start kernel" << endl;
    cl_event enqueue_kernel;
    err = clEnqueueTask(context.commands, context.kernel, 0, NULL, &enqueue_kernel);
    if (err)
    {
        log << "Error: Failed to execute kernel! " << err << endl;
        log.flush();
        exit(-1);
    }

    //wait for the end of computation
    clWaitForEvents(1, &enqueue_kernel);


    // Read back the results from the device to verify the output
    //
    cl_event readevent;
    err = clEnqueueReadBuffer(context.commands, context.out_buff, CL_TRUE, 0,
                              sizeof(float) * num_iter * 100, __fpga_output_buffer, 0, NULL, &readevent );
    if (err != CL_SUCCESS)
    {
        log << "Error: Failed to read the output array! " << err << endl;
        log.flush();
        exit(-1);
    }

    clWaitForEvents(1, &readevent);


    essential_sizes.push_back(0);
    // Fetch output and populate essential matrices
    for(int i = 0; i < num_iter; i++) {
        for(int j = 0; j < 10; j++) {
            int offset = i*100 + j*10;
            if(isnan(__fpga_output_buffer[offset])) {
                break;
            }
            if( __fpga_output_buffer[offset] > 0) {
                opengv::essential_t essential;
                for(int k = 0; k < 9; k++) {
                    essential(k % 3, k / 3) = __fpga_output_buffer[offset + 1 + k];
                }
                essentials.push_back(essential);
            }
        }
        essential_sizes.push_back(essentials.size());
    }
}

void release_fpga_kernel(kernel_context & context)
{
    clReleaseMemObject(context.input_a_buff);
    clReleaseMemObject(context.input_b_buff);
    clReleaseMemObject(context.out_buff);
    clReleaseProgram(context.program);
    clReleaseKernel(context.kernel);
    clReleaseCommandQueue(context.commands);
    clReleaseContext(context.context);
}