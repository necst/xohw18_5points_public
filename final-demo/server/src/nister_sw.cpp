#include "../global.h"
#include "../includes/nister_sw.h"
#include "../includes/utils.h"
#include <opengv/relative_pose/methods.hpp>
#include <opengv/relative_pose/modules/main.hpp>
#include <Eigen/StdVector>

using namespace std;
using namespace Eigen;
using namespace opengv;

void my_fivept_nister(
        const opengv::relative_pose::RelativeAdapterBase & adapter, essentials_t & essentials)
{
    Eigen::MatrixXd Q(POINTS_PER_ITER,9);
    for( size_t i = 0; i < POINTS_PER_ITER; i++ )
    {
        //bearingVector_t f = adapter.getBearingVector1(indices[i]);
        //bearingVector_t fprime = adapter.getBearingVector2(indices[i]);
        //Nister's algorithm is computing the inverse transformation, so we simply
        //invert the input here
        bearingVector_t f = adapter.getBearingVector2(i);
        bearingVector_t fprime = adapter.getBearingVector1(i);
        Eigen::Matrix<double,1,9> row;
        row <<  f[0]*fprime[0], f[1]*fprime[0], f[2]*fprime[0],
                f[0]*fprime[1], f[1]*fprime[1], f[2]*fprime[1],
                f[0]*fprime[2], f[1]*fprime[2], f[2]*fprime[2];
        Q.row(i) = row;
    }

    Eigen::JacobiSVD< Eigen::MatrixXd > SVD(Q, Eigen::ComputeFullV );
    Eigen::Matrix<double,9,4> EE = SVD.matrixV().block(0,5,9,4);

    relative_pose::modules::fivept_nister_main(EE,essentials);
}

void multi_nister_sw(point *p1, point *p2, size_t num_iter, opengv::essentials_t & essentials,
                     essential_sizes_t & essential_sizes)
{
    bearingVectors_t b1(POINTS_PER_ITER), b2(POINTS_PER_ITER);
    relative_pose::CentralRelativeAdapter relAdapter(b1, b2);
    essential_sizes.push_back(0);

    for (int i = 0; i < num_iter; i++) {

        // load points into the bearing vectors
        for (int j = 0; j < POINTS_PER_ITER; j++) {
            int offset = i * POINTS_PER_ITER + j;
            b1[j] = Eigen::Vector3d(p1[offset].x, p1[offset].y, 1.0);
            b2[j] = Eigen::Vector3d(p2[offset].x, p2[offset].y, 1.0);
            b1[j] = b1[j] / b1[j].norm();
            b2[j] = b2[j] / b2[j].norm();
        }

        // run nister algorithm
        my_fivept_nister(relAdapter, essentials);
        essential_sizes.push_back(essentials.size());
    }

}