#include "../includes/ransac.h"
#include <fstream>
#include <opengv/triangulation/methods.hpp>
#include <opengv/relative_pose/CentralRelativeAdapter.hpp>
#include <Eigen/StdVector>

using namespace std;
using namespace Eigen;
using namespace opengv;

void gen_5_point_problems(point *p1_in, point *p2_in, size_t n_points,
                          point *p1_out, point *p2_out, size_t n_problems, size_t *out_indices,
                          int *rand_numbers, size_t num_rand_numbers, size_t &rand_offset,  ofstream & log)
{
    point tmp1, tmp2;
    size_t sel_index;
    size_t *tmp_indices;
    int i, j;

    if(n_points < TOTAL_POINTS_PER_ITER) {
        log << "RANSAC ERROR: required at least " << TOTAL_POINTS_PER_ITER
            << " but only " << n_points << " provided." << endl;
        log.flush();
        exit(1);
    }

    tmp_indices = new size_t[n_points];
    for(int j = 0; j < n_points; j++) {
        tmp_indices[j] = j;
    }

    for(i = 0; i < n_problems; i++) {
        for(j = 0; j < POINTS_PER_ITER; j++) {
            // get random points p1 and p2 in the interval [j, n_points - 1]
            size_t rpos = rand_numbers[rand_offset] % (n_points - j) + j;
            rand_offset = (rand_offset + 1) % num_rand_numbers;
            tmp1 = p1_in[rpos];
            tmp2 = p2_in[rpos];

            // assign points to the current 5-point problem at position j
            p1_out[i * POINTS_PER_ITER + j] = tmp1;
            p2_out[i * POINTS_PER_ITER + j] = tmp2;

            // swap the selected point in the input array to ensure no duplicated points in the 5-point problem
            p1_in[rpos] = p1_in[j];
            p2_in[rpos] = p2_in[j];
            p1_in[j] = tmp1;
            p2_in[j] = tmp2;

            // store index
            sel_index = tmp_indices[rpos];
            out_indices[i * TOTAL_POINTS_PER_ITER + j] = sel_index;
            tmp_indices[rpos] = tmp_indices[j];
            tmp_indices[j] = sel_index;
        }
        for(; j < TOTAL_POINTS_PER_ITER; j++) {
            // get random index in the interval [j, n_points - 1]
            size_t rpos = rand_numbers[rand_offset] % (n_points - j) + j;
            rand_offset = (rand_offset + 1) % num_rand_numbers;

            // store index
            sel_index = tmp_indices[rpos];
            out_indices[i * TOTAL_POINTS_PER_ITER + j] = sel_index;
            tmp_indices[rpos] = tmp_indices[j];
            tmp_indices[j] = sel_index;
        }
    }

    delete tmp_indices;
}


bool recover_transformation(opengv::essentials_t & essentials, essential_sizes_t & essential_sizes,
                            size_t * indices,
                            relative_pose::CentralRelativeAdapter & adapter, size_t ransac_iter,
                            opengv::transformation_t & outModel, opengv::transformation_t & inverseOutModel)
{
    size_t tests = (size_t) essential_sizes[ransac_iter + 1] - essential_sizes[ransac_iter];

    if(tests == 0) {
        return false;
    }

    transformations_t transformations(tests * 4);
    transformations_t inverseTransformations(tests * 4);

    //now decompose each essential matrix into transformations and find the
    //right one
    Eigen::Matrix3d W = Eigen::Matrix3d::Zero();
    W(0,1) = -1;
    W(1,0) = 1;
    W(2,2) = 1;

    size_t indices_offset = ransac_iter * TOTAL_POINTS_PER_ITER;

    for(size_t i = (size_t) essential_sizes[ransac_iter], o = 0; i < essential_sizes[ransac_iter + 1]; i++, o++)
    {
        // decompose
        Eigen::MatrixXd tempEssential = essentials[i];
        Eigen::JacobiSVD< Eigen::MatrixXd > SVD(
                tempEssential,
                Eigen::ComputeFullV | Eigen::ComputeFullU );
        Eigen::VectorXd singularValues = SVD.singularValues();

        // check for bad essential matrix
        if( singularValues[2] > 0.001 ) {};
        // continue; //singularity constraints not applied -> removed because too harsh
        if( singularValues[1] < 0.75 * singularValues[0] ) {};
        // continue; //bad essential matrix -> removed because too harsh

        // maintain scale
        double scale = singularValues[0];

        // get possible rotation and translation vectors
        rotation_t Ra = SVD.matrixU() * W * SVD.matrixV().transpose();
        rotation_t Rb = SVD.matrixU() * W.transpose() * SVD.matrixV().transpose();
        translation_t ta = scale*SVD.matrixU().col(2);
        translation_t tb = -ta;

        // change sign if det = -1
        if( Ra.determinant() < 0 ) Ra = -Ra;
        if( Rb.determinant() < 0 ) Rb = -Rb;

        //derive transformations
        transformation_t transformation;
        transformation.col(3) = ta;
        transformation.block<3,3>(0,0) = Ra;
        transformations[o*4 + 0] = transformation;
        transformation.col(3) = ta;
        transformation.block<3,3>(0,0) = Rb;
        transformations[o*4 + 1] = transformation;
        transformation.col(3) = tb;
        transformation.block<3,3>(0,0) = Ra;
        transformations[o*4 + 2] = transformation;
        transformation.col(3) = tb;
        transformation.block<3,3>(0,0) = Rb;
        transformations[o*4 + 3] = transformation;

        // derive inverse transformations
        for(size_t j = 0; j < 4; j++)
        {
            transformation_t inverseTransformation;
            inverseTransformation.block<3,3>(0,0) =
                    transformations[o*4 + j].block<3,3>(0,0).transpose();
            inverseTransformation.col(3) =
                    -inverseTransformation.block<3,3>(0,0)*transformations[o*4 + j].col(3);
            inverseTransformations[o*4 + j] = inverseTransformation;
        }
    }

    double bestQuality = 1000000.0;
    int bestQualityIndex = -1;

    for(size_t i = 0; i < tests; i++)
    {
        // collect qualities for each of the four solutions solution
        Eigen::Matrix<double,4,1> p_hom;
        p_hom[3] = 1.0;

        for(size_t j = 0; j<4; j++)
        {
            // prepare variables for triangulation and reprojection
            adapter.sett12(transformations[i*4 + j].col(3));
            adapter.setR12(transformations[i*4 + j].block<3,3>(0,0));

            // go through all features and compute quality of reprojection
            double quality = 0.0;

            for( int k = 0; k < TOTAL_POINTS_PER_ITER; k++ )
            {
                p_hom.block<3, 1>(0, 0) =
                        opengv::triangulation::triangulate2(adapter, indices[indices_offset + k]);
                bearingVector_t reprojection1 = p_hom.block<3, 1>(0, 0);
                bearingVector_t reprojection2 = inverseTransformations[i*4 + j] * p_hom;
                reprojection1 = reprojection1 / reprojection1.norm();
                reprojection2 = reprojection2 / reprojection2.norm();
                bearingVector_t f1 = adapter.getBearingVector1(k);
                bearingVector_t f2 = adapter.getBearingVector2(k);

                // bearing-vector based outlier criterium (select threshold accordingly):
                // 1-(f1'*f2) = 1-cos(alpha) \in [0:2]
                double reprojError1 = 1.0 - (f1.dot(reprojection1));
                double reprojError2 = 1.0 - (f2.dot(reprojection2));

                double error = reprojError1 + reprojError2;
                quality += error;

                if(bestQualityIndex != -1 && quality >= bestQuality) {
                    break;
                }
            }

            // is quality better? (lower)
            if( quality < bestQuality )
            {
                bestQuality = quality;
                bestQualityIndex = i*4 + j;
            }
        }
    }

    if( bestQualityIndex == -1 ) {
        return false; // no solution found
    }

    outModel.col(3) = transformations[bestQualityIndex].col(3);
    outModel.block<3,3>(0,0) = transformations[bestQualityIndex].block<3,3>(0,0);
    inverseOutModel.col(3) = inverseTransformations[bestQualityIndex].col(3);
    inverseOutModel.block<3,3>(0,0) = inverseTransformations[bestQualityIndex].block<3,3>(0,0);
    return true;
}




double compute_quality(relative_pose::CentralRelativeAdapter & adapter,
                       opengv::transformation_t & model, opengv::transformation_t & inverseSolution,
                       double ransac_threshold, double best_quality)
{
    translation_t translation = model.col(3);
    rotation_t rotation = model.block<3,3>(0,0);
    adapter.sett12(translation);
    adapter.setR12(rotation);

    Eigen::Matrix<double,4,1> p_hom;
    p_hom[3] = 1.0;

    double quality = 0;

    for(size_t i = 0; i < adapter.getNumberCorrespondences(); i++) {
        p_hom.block<3,1>(0,0) =
        opengv::triangulation::triangulate2(adapter, i);
        bearingVector_t reprojection1 = p_hom.block<3,1>(0,0);
        bearingVector_t reprojection2 = inverseSolution * p_hom;
        reprojection1 = reprojection1 / reprojection1.norm();
        reprojection2 = reprojection2 / reprojection2.norm();
        bearingVector_t f1 = adapter.getBearingVector1(i);
        bearingVector_t f2 = adapter.getBearingVector2(i);

        //bearing-vector based outlier criterium (select threshold accordingly):
        //1-(f1'*f2) = 1-cos(alpha) \in [0:2]
        double reprojError1 = 1.0 - (f1.transpose() * reprojection1);
        double reprojError2 = 1.0 - (f2.transpose() * reprojection2);

        double error = reprojError1 + reprojError2;
        if(error > ransac_threshold) {
            error = 1;
        }

        quality += error;

        if(quality > best_quality) {
            return quality;
        }
    }

    return quality;
}


bool ransac(opengv::essentials_t & essentials, essential_sizes_t & essential_sizes,
            size_t * indices, point *p1, point *p2, size_t num_points,
            opengv::transformation_t & outModel, double ransac_threshold, double & best_quality)
{
    // prepare bearing vectors and central relative adapter
    bearingVectors_t b1(num_points), b2(num_points);
    relative_pose::CentralRelativeAdapter adapter(b1, b2);

    for (int j = 0; j < num_points; j++) {
        b1[j] = Eigen::Vector3d(p1[j].x, p1[j].y, 1.0);
        b2[j] = Eigen::Vector3d(p2[j].x, p2[j].y, 1.0);
        b1[j] = b1[j] / b1[j].norm();
        b2[j] = b2[j] / b2[j].norm();
    }

    opengv::transformation_t inverseModel;

    best_quality = num_points * 10;
    int best_iteration = -1;

    for(size_t i = 0; i < essential_sizes.size() - 1; i++) {
        bool result = recover_transformation(essentials, essential_sizes, indices, adapter, i, outModel, inverseModel);
        if(result) {
            double cur_quality = compute_quality(adapter, outModel, inverseModel, ransac_threshold, best_quality);
            if(cur_quality < best_quality) {
                best_quality = cur_quality;
                best_iteration = i;
            }
        }
    }

    if(best_iteration >= 0) {
        // recompute best transformation
        recover_transformation(essentials, essential_sizes, indices, adapter, best_iteration, outModel, inverseModel);
        return true;
    }

    return false;
}



bool recover_best_transformation(opengv::essentials_t & essentials, essential_sizes_t & essential_sizes,
                                 size_t * indices, point *p1, point *p2, size_t num_points,
                                 opengv::transformation_t & outModel, double ransac_threshold, double & quality)
{
    bearingVectors_t b1(num_points), b2(num_points);
    relative_pose::CentralRelativeAdapter adapter(b1, b2);

    for (int j = 0; j < num_points; j++) {
        b1[j] = Eigen::Vector3d(p1[j].x, p1[j].y, 1.0);
        b2[j] = Eigen::Vector3d(p2[j].x, p2[j].y, 1.0);
        b1[j] = b1[j] / b1[j].norm();
        b2[j] = b2[j] / b2[j].norm();
    }

    //now decompose each essential matrix into transformations and find the
    //right one
    Eigen::Matrix3d W = Eigen::Matrix3d::Zero();
    W(0,1) = -1;
    W(1,0) = 1;
    W(2,2) = 1;

    Eigen::Matrix3d Wtrasp = W.transpose();

    double bestQuality = 1000000.0;
    int bestQualityIndex = -1;
    int bestQualitySubindex = -1;

    for(size_t i = 0; i < essentials.size(); i++)
    {
        // decompose
        Eigen::MatrixXd tempEssential = essentials[i];
        Eigen::JacobiSVD< Eigen::Matrix<double, 3, 3>> SVD(
                tempEssential,
                Eigen::ComputeFullV | Eigen::ComputeFullU );
        Eigen::VectorXd singularValues = SVD.singularValues();

        // check for bad essential matrix
        if( singularValues[2] > 0.001 ) {};
        // continue; //singularity constraints not applied -> removed because too harsh
        if( singularValues[1] < 0.75 * singularValues[0] ) {};
        // continue; //bad essential matrix -> removed because too harsh

        // maintain scale
        double scale = singularValues[0];

        // get possible rotation and translation vectors
        rotation_t Ra = SVD.matrixU() * W * SVD.matrixV().transpose();
        rotation_t Rb = SVD.matrixU() * Wtrasp * SVD.matrixV().transpose();
        translation_t ta = scale*SVD.matrixU().col(2);
        translation_t tb = -ta;

        // change sign if det = -1
        if( Ra.determinant() < 0 ) Ra = -Ra;
        if( Rb.determinant() < 0 ) Rb = -Rb;

        //derive transformations
        transformation_t transformation;
        transformations_t transformations;
        transformation.col(3) = ta;
        transformation.block<3,3>(0,0) = Ra;
        transformations.push_back(transformation);
        transformation.col(3) = ta;
        transformation.block<3,3>(0,0) = Rb;
        transformations.push_back(transformation);
        transformation.col(3) = tb;
        transformation.block<3,3>(0,0) = Ra;
        transformations.push_back(transformation);
        transformation.col(3) = tb;
        transformation.block<3,3>(0,0) = Rb;
        transformations.push_back(transformation);

        // derive inverse transformations
        transformations_t inverseTransformations;
        for(size_t j = 0; j < 4; j++)
        {
            transformation_t inverseTransformation;
            inverseTransformation.block<3,3>(0,0) =
                    transformations[j].block<3,3>(0,0).transpose();
            inverseTransformation.col(3) =
                    -inverseTransformation.block<3,3>(0,0)*transformations[j].col(3);
            inverseTransformations.push_back(inverseTransformation);
        }

        // collect qualities for each of the four solutions solution
        Eigen::Matrix<double,4,1> p_hom;
        p_hom[3] = 1.0;

        for(size_t j = 0; j<4; j++)
        {
            // prepare variables for triangulation and reprojection
            adapter.sett12(transformations[j].col(3));
            adapter.setR12(transformations[j].block<3,3>(0,0));

            // go through all features and compute quality of reprojection
            double quality = 0.0;

            for( int k = 0; k < adapter.getNumberCorrespondences(); k++ )
            {
                p_hom.block<3, 1>(0, 0) =
                        opengv::triangulation::triangulate2(adapter, k);
                bearingVector_t reprojection1 = p_hom.block<3, 1>(0, 0);
                bearingVector_t reprojection2 = inverseTransformations[j] * p_hom;
                reprojection1 = reprojection1 / reprojection1.norm();
                reprojection2 = reprojection2 / reprojection2.norm();
                bearingVector_t f1 = adapter.getBearingVector1(k);
                bearingVector_t f2 = adapter.getBearingVector2(k);

                // bearing-vector based outlier criterium (select threshold accordingly):
                // 1-(f1'*f2) = 1-cos(alpha) \in [0:2]
                double reprojError1 = 1.0 - (f1.dot(reprojection1));
                double reprojError2 = 1.0 - (f2.dot(reprojection2));

                double error = reprojError1 + reprojError2;
                if (error > ransac_threshold) {
                    error = ransac_threshold;
                }
                quality += error;

                if(bestQualityIndex != -1 && quality >= bestQuality) {
                    break;
                }
            }

            // is quality better? (lower)
            if( quality < bestQuality )
            {
                bestQuality = quality;
                bestQualityIndex = i;
                bestQualitySubindex = j;
            }
        }
    }

    if( bestQualityIndex == -1 )
        return false; // no solution found
    else
    {
        // rederive the best solution
        // decompose
        Eigen::MatrixXd tempEssential = essentials[bestQualityIndex];
        Eigen::JacobiSVD< Eigen::MatrixXd > SVD(
                tempEssential,
                Eigen::ComputeFullV | Eigen::ComputeFullU );
        const Eigen::VectorXd singularValues = SVD.singularValues();

        // maintain scale
        const double scale = singularValues[0];

        // get possible rotation and translation vectors
        translation_t translation;
        rotation_t rotation;

        switch(bestQualitySubindex)
        {
            case 0:
                translation = scale*SVD.matrixU().col(2);
                rotation = SVD.matrixU() * W * SVD.matrixV().transpose();
                break;
            case 1:
                translation = scale*SVD.matrixU().col(2);
                rotation = SVD.matrixU() * W.transpose() * SVD.matrixV().transpose();
                break;
            case 2:
                translation = -scale*SVD.matrixU().col(2);
                rotation = SVD.matrixU() * W * SVD.matrixV().transpose();
                break;
            case 3:
                translation = -scale*SVD.matrixU().col(2);
                rotation = SVD.matrixU() * W.transpose() * SVD.matrixV().transpose();
                break;
            default:
                return false;
        }

        // change sign if det = -1
        if( rotation.determinant() < 0 ) rotation = -rotation;

        // output final selection
        outModel.block<3,3>(0,0) = rotation;
        outModel.col(3) = translation;
    }

    quality = bestQuality;
    return true;
}