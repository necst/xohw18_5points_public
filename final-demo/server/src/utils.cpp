#include "../includes/utils.h"
#include <iostream>

double timediff( const struct timeval &t1, const struct timeval &t2 )
{
    timeval ret;
    ret.tv_sec = t1.tv_sec - t2.tv_sec;
    if( t1.tv_usec < t2.tv_usec )
    {
        ret.tv_sec--;
        ret.tv_usec = t1.tv_usec - t2.tv_usec + 1000000;
    }
    else
        ret.tv_usec = t1.tv_usec - t2.tv_usec;

    return ret.tv_sec + ret.tv_usec * 1e-6;
}

void init_random_numbers(int seed, size_t num_values, int *numbers)
{
    srand(seed);
    for(int i = 0; i < num_values; i++) {
        numbers[i] = rand();
    }
}