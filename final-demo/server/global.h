#ifndef AWS_DEMO_GLOBAL_H
#define AWS_DEMO_GLOBAL_H

#include <vector>

#define MAX_INPUT_POINTS 1024
#define NUM_RANDOM_NUMBERS 1000000
#define MAX_RANSAC_ITER (1024*16)
#define POINTS_PER_ITER 5
#define EXTRA_RANSAC_POINTS 3
#define TOTAL_POINTS_PER_ITER (POINTS_PER_ITER + EXTRA_RANSAC_POINTS)

// The following define is uber important
// it should match the same value as used when compiling
// opengv!
#define EIGEN_MALLOC_ALREADY_ALIGNED 0

typedef struct {
    float x;
    float y;
} point;

typedef enum {
    nister_sw,
    nister_fpga
} five_point_algorithm;

typedef std::vector<int> essential_sizes_t;

#endif //AWS_DEMO_GLOBAL_H
