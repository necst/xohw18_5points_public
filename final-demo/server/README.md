# 5-points demo server

This folder contains the scripts and source code to run the 5-points server.
The rerver application consists of:

- 5_points.awsxclbin (not included in this repo, you can generate it starting from the provided xclbin bitstream file using the AWS CLI).
- Source code to run both the FPGA-accelerated 5-points algorithm as well as the original 5-point software code
- Support scripts needed to install all the software dependencies and execute the server. The server application is a pure C/C++ application executed with websocketd (*https://github.com/joewalnes/websocketd*). Websocketd wraps the application standard input and standard output with websockets that allow a simple communication with the web-based client written in html/javascript.

# BUILD and run the server on a AWS F1 instance

The setup has been tested using SDAccel 2017.1, hence, when selecting the F1 instance you should select the appropriate F1 instance version that supports SDAccel 2017.1.

- start a new AWS F1 instance with support for SDAccel 2017.1
- configure the firewall setting of the instance and ensure that port 8080 is open (this will be used for data exchange between the server and the client). If you wish to use another port you can do so by modifying the **run.sh** script and specify a different port
- generate the **5_points.awsxclbin** file and copy it into the server folder
- copy the content of the server folder to the AWS F1 instance under */home/centos*
- run the **aws-f1-install.sh** script to install all the software dependencies (namely: cmake3, eigen, opengv and websocketd) and to build the host code
- run the server by launching the script: **run.sh** (the server will listen to port 8080 by default)


# BUILD and run the server locally on a centos machine

It is also possibile to run the server locally with support for 5-point software execution only (FPGA acceleration currently requires an AWS F1 instance).
To run the server locally on a **centos linux machine**, you can follow these steps (note: it is also possible to run the server on different systems, however you might need to do the software installation and build manually):

- cd into the server folder, then run the script *local-install.sh* to install all the software dependencies (namely: cmake3, eigen, opengv and websocketd) and to build the server code with support for software execution only.
- run the server code by launching the script: **run-local.sh** (the server will listen to port 8080)