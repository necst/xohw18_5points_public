diff --git a/src/math/Sturm.cpp b/src/math/Sturm.cpp
index 5c9678c..0fb1818 100644
--- a/src/math/Sturm.cpp
+++ b/src/math/Sturm.cpp
@@ -74,6 +74,9 @@ opengv::math::Bracket::dividable( double eps ) const
     return false;
   if( numberRoots() == 0 )
     return false;
+  double center = (_upperBound + _lowerBound) / 2.0;
+  if( center == _upperBound || center == _lowerBound)
+    return false;
   return true;
 }
 
