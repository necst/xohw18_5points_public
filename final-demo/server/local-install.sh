#!/bin/bash

# install cmake3
sudo yum -y install epel-release
yum install -y cmake3

# install eigen
git clone https://github.com/eigenteam/eigen-git-mirror eigen
cd eigen
git checkout 3.3.5
cd ..
sudo mv eigen /usr/local/include/eigen

# install opengv
git clone https://github.com/laurentkneip/opengv.git opengv
cd opengv
git checkout 0b2017d23bbbb9a39d3f508ad2cd7042a58be682
git apply ../server/opengv-patch.txt 
mkdir build
cd build
cmake3 ../
make
sudo make install
cd ../../

# install websocketd
wget https://github.com/joewalnes/websocketd/releases/download/v0.3.0/websocketd-0.3.0-linux_amd64.zip
unzip websocketd-0.3.0-linux_amd64.zip

# build server
mkdir build
cd build
cmake3 -DAWS_DEPLOY=0 ../
make

