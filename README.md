Team number: XOHW18-410
Project name: 5 Points to Rule Them All
Date: 06/30/2018
Version of uploaded archive: 1.0

University name: Politecnico di Milano
Supervisor name: Marco Domenico Santambrogio
Supervisor e-mail: marco.santambrogio@polimi.it
Participant(s): Marco Rabozzi
Email: marco.rabozzi@polimi.it
Participant(s): Emanuele Del Sozzo
Email: emanuele.delsozzo@polimi.it
Participant(s): Lorenzo Di Tucci
Email: lorenzo.ditucci@polimi.it

Board used: Xilinx Virtex Ultrascale+ VU9P FPGA
Vivado/SDAccel Version: 2017.1.op
Brief description of project: this work presents an implementation to solve the 5-point relative pose problem accelerated on Field Programmable Gate Array (FPGA). The proposed architecture implements the classical Nister’s algorithm as a deep pipeline deployed on a AWS F1 instance and outperforms software implementations by a factor ranging from 7.2X to 233X. Furthermore, it achieves a speedup of 64.2X compared to the Nister’s software implementation with comparable accuracy.

Description of archive (explain directory structure, documents and source files):
The archive is structured as follows:

- load-store-architecture-compiler/	folder of the load-store architecture compiler
- report/report.pdf					report of the project
- SDx/								folder of the SDx project 
- final-demo						folder containing the demo shown at the XOHW18 awards ceremony

For the instructions about how to build the SDx project and use the load-store architecture compiler, please refer to the readme files within the respective folders

Useful links:
- YouTube: 				https://youtu.be/UDGWGNdglFs
- BitBucket: 			https://bitbucket.org/necst/xohw18_5points_public/
- Dropbox (bitstream):	https://www.dropbox.com/sh/0wggmxtmuumzz4w/AABDOLAIDZqKUQJCY7eE5AOha?dl=0