#!/bin/bash

# run the following scripts to auto generate the cores for computing
# matrices A1, A2 and the coefficients of the 10-th degree polynomial

python compile.py coeffA1_in.h e 36 A 100 14 500 CFA1_ float double float > core_coeffs_A1.h
python compile.py coeffA2_in.h e 36 A 100 14 500 CFA2_ float double float > core_coeffs_A2.h
python compile.py coeffC_in.h b 39 c 11 14 500 CFC_ float double float > core_coeffs_C.h
