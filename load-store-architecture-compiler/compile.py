"""
authors:
Marco Rabozzi (marco.rabozzi@polimi.it)
Emanuele Del Sozzo (emanuele.delsozzo@polimi.it)
Lorenzo Di Tucci (lorenzo.ditucci@polimi.it)

Simple C to Vivado HLS C compiler for load-store parallel architectures.
The compiler supports simple excerpt of C code such as the ones provided
in the files: coeffA1_in.h, coeffA2_in.h, coeffC_in.h.

The compiler generates a dataflow graph representation of the computation
and partitions the graph on multiple load-store core according to the
latency requirements.

Currently, the type of supported operations are floating-point additions,
subtractions and multiplications.

The compiler assumes one input array and one output array.

All the operations should be single statement assignment.
"""

import sys
import re
import copy

"""
extract the dataflow graph from a simple excerpt of a C code.
supported examples are the files: coeffA1_in.h, coeffA2_in.h, coeffC_in.h
"""
def extractGraph(filename):
	
	operations = {}
	prog = re.compile('.*?([a-zA-Z0-9\[\]]+)[\ ]*=[\ ]*([a-zA-Z0-9\[\]\.]+)[\ ]*([*\+-])[\ ]*([a-zA-Z0-9\[\]\.]+).*')
	with open(filename) as fp:
		for l in fp:
			res = prog.match(l)
			if res:
				val = res.groups(1)
				operations[val[0]] = {'l' : val[1], 'r' : val[3], 'op' : val[2], 'level' : -1}

	# assign level 0 to operations relying on base input only
	instrLeft = len(operations)
	for name in operations:
		op = operations[name]
		if not op['l'] in operations and not op['r'] in operations:
			op['level'] = 0
			instrLeft -= 1

	# computes all other levels
	while instrLeft > 0:
		for name in operations: 
			op = operations[name]
			if op['level'] == -1:
				ll = -2
				lr = -2

				if op['l'] in operations:
					if operations[op['l']]['level'] != -1:
						ll = operations[op['l']]['level']
				else:
					ll = -1
				if op['r'] in operations:
					if operations[op['r']]['level'] != -1:
						lr = operations[op['r']]['level']
				else:
					lr = -1

				if ll != -2 and lr != -2:
					op['level'] = max(ll, lr) + 1
					instrLeft -= 1


	return operations


"""
extract a subgraph rooted at operation opName
"""
def extractSubgraph(operations, opName):
	if opName in operations:
		lTree = extractSubgraph(operations, operations[opName]['l'])
		rTree = extractSubgraph(operations, operations[opName]['r'])
		lTree.update(rTree)
		lTree[opName] = operations[opName]
		return lTree
	return {}

"""
extract a partition of the graph from a set of roots operations
"""
def extractPartion(operations, out_ar, out_size, out_from, out_to):
	partition = {}

	for out_idx in range(out_from, out_to):
		var_name = out_ar + '[' + str(out_idx) + ']'
		tree = extractSubgraph(operations, var_name)
		for el in tree:
			partition[el] = tree[el]

	return partition

"""
partition the original dataflow graph in a set of dataflow graphs
that overall perform the same computation

notice that the a node might appear in multiple subgraph if it is
needed for its computation
"""
def partitioning(operations, out_ar, out_size, factor):
	partitions = []
	for r in range(factor):
		partitions.append({})

	for out_idx in range(0, out_size):
		p_idx = out_idx % factor
		var_name = out_ar + '[' + str(out_idx) + ']'
		tree = extractSubgraph(operations, var_name)
		for el in tree:
			partitions[p_idx][el] = tree[el]

	return partitions


"""
simple heuristic schedule for the operations within a dataflow graph.
It assumes each operation performed at every clock cycle and a latency
to complete the operation of op_latency.
The schedule takes into account latency and data dapendencies.
If needed and if no operations are available for scheduling, nop instructions
are inserted.
"""
def schedule_he(operations, op_latency):
	# estimate worst case schedule
	num_ops = len(operations)
	num_levels = max(operations[op]['level'] for op in operations) + 1
	makespan = num_ops + num_levels*(op_latency-1)
	
	for op in operations:
		operations[op]['name'] = op

	op_list = [operations[op] for op in operations]

	op_list.sort(key = lambda x: x['level'])

	curTime = 0
	opTime = {}

	for o in op_list:
		
		# check how far we have to start
		lastDep = -op_latency
		if o['l'] in operations:
			lastDep = opTime[o['l']]
		if o['r'] in operations:
			lastDep = max(lastDep, opTime[o['r']])

		curTime = max(curTime, lastDep + op_latency)

		opTime[o['name']] = curTime
		curTime += 1

	return opTime


"""
computes the number of clock cycles needed to complete a schedule
of a dataflow graph
"""
def getScheduleLen(opTime):
	return max(opTime[el] for el in opTime) + 1

"""
generates the instructions in simple hex format from the schedule:

------------------------------------------------------------------------------------
| op_code (2 bit) | addr op 1 (10 bit) | addr op 2 (10 bit) | addr result (10 bit) |
------------------------------------------------------------------------------------
"""
def genInstruction(addr1, addr2, addr3, op):
	opMap = {
		'*' : 0,
		'+' : 1,
		'-' : 2,
		'nop' : 3
	}

	opVal = opMap[op]
	return '0x{0:08X}'.format((opVal << 30) + (addr1 << 20) + (addr2 << 10) + addr3)


"""
generates the code for the load store architecture starting from the 
load-store-template.c file
"""
def genCode(filename, schedules, partitions, partitions_output, in_ar, in_size, \
	out_ar, out_size, op_latency, custom_prefix, data_type_in, data_type_internal, data_type_out):

	with open('load-store-template.c') as fp:
		lines = fp.readlines()
	code = "".join(lines)

	# extract all needed constants
	registers = {}
	curReg = 0
	for i in range(in_size):
		registers[in_ar + '[' + str(i) + ']'] = curReg
		curReg += 1

	# input registers
	consts = []
	for p in partitions:
		for name in p:
			op = p[name]
			if op['l'] not in p and op['l'] not in registers:
				registers[op['l']] = curReg
				consts.append("\ttmp1[" + str(curReg) + "] = " + op['l'] + ";")
				consts.append("\ttmp2[" + str(curReg) + "] = " + op['l'] + ";")
				curReg += 1
			if op['r'] not in p and op['r'] not in registers:
				registers[op['r']] = curReg
				consts.append("\ttmp1[" + str(curReg) + "] = " + op['l'] + ";")
				consts.append("\ttmp2[" + str(curReg) + "] = " + op['l'] + ";")
				curReg += 1

	maxInstr = max([getScheduleLen(s) for s in schedules])

	outRegOffset = maxInstr + len(consts) + in_size
	outOffsets = []
	maxCoreOut = max(partitions_outputs)

	defines = []
	defines.append("#define CFA_NUM_CORES " + str(len(partitions)))
	defines.append("#define CFA_ISTR " + str(maxInstr))
	defines.append("#define CFA_NUM_REGS " + str(outRegOffset + maxCoreOut))
	defines.append("#define CFA_IN_SIZE " + str(in_size))
	defines.append("#define CFA_CONSTS " + str(len(consts)))
	defines.append("#define CFA_OUT_SIZE " + str(out_size))

	# generate instructions
	baseOffset = curReg
	commonRegs = copy.copy(registers)

	allOpCodes = []

	out_template = """
	for(int i = {start}; i < {end}; i++) {
		#pragma HLS PIPELINE
		out[i] = out_parts[{p_id}][i-{start}];
	}
	"""

	curStart = 0
	outputs = []
	for p_id, p in enumerate(partitions):
		output = out_template \
			.replace('{p_id}', str(p_id)) \
			.replace('{start}', str(curStart)) \
			.replace('{end}', str(curStart + partitions_outputs[p_id]))
		outputs.append(output)
		outOffsets.append(curStart)

		curReg = baseOffset
		registers = copy.copy(commonRegs)
		schedule = schedules[p_id]

		# prepopulate output registers
		outReg = outRegOffset
		for i in range(curStart, partitions_outputs[p_id] + curStart):
			registers[out_ar + '[' + str(i) + ']'] = outReg
			outReg += 1

		curStart += partitions_outputs[p_id]


		instrTime = []
		for i in range(maxInstr):
			instrTime.append({'op' : 'nop'})

		for name in p:
			op = p[name]
			ts = schedule[name]
			instrTime[ts] = op

		opCodes = []
		for i in range(maxInstr):

			addr1 = 0
			addr2 = 0
			addr3 = 0
			op = instrTime[i]

			if op['op'] != 'nop':
				addr1 = registers[op['l']]
				addr2 = registers[op['r']]
				if not op['name'] in registers:
					addr3 = curReg
					registers[op['name']] = curReg
					curReg += 1
				else:
					addr3 = registers[op['name']]
			opCodes.append(genInstruction(addr1, addr2, addr3, op['op']))
		allOpCodes.append("\t\t{" + ", ".join(opCodes) + "}")

	definesStr = "\n".join(defines)
	constsStr = "\n".join(consts)
	instrStr = ",\n".join(allOpCodes)
	outputsStr = "\n".join(outputs)
	num_outputsStr = ",".join([str(el) for el in partitions_output])
	output_offsetsStr = ",".join([str(el) for el in outOffsets])

	code = code.replace('{defines}', definesStr)
	code = code.replace('{consts}', constsStr)
	code = code.replace('{instrs}', instrStr)
	code = code.replace('{num_outputs}', num_outputsStr)
	code = code.replace('{output_offsets}', output_offsetsStr)
	code = code.replace('{original_filename}', filename)
	code = code.replace('{data_type_in}', data_type_in)
	code = code.replace('{data_type_internal}', data_type_internal)
	code = code.replace('{data_type_out}', data_type_out)
	code = code.replace('CFA_', custom_prefix)

	return code
	

# read imput parameters
if len(sys.argv) <= 11:
	print('USAGE: COEFF_IN.h INPUT_NAME INPUT_SIZE OUTPUT_NAME OFFSET_OUTPUT OUTPUT_SIZE OP_LATENCY TARGET_STAGE_LAT CUSTOM_DEFINE_PREFIX DATA_TYPE_IN DATA_TYPE_INTERNAL DATA_TYPE_OUT')
	sys.exit(1)

filename = sys.argv[1]
in_ar = sys.argv[2]
in_size = int(sys.argv[3])
out_ar = sys.argv[4]
out_size = int(sys.argv[5])
op_latency = int(sys.argv[6])
target_lat = int(sys.argv[7])
custom_prefix = sys.argv[8]
data_type_in = sys.argv[9]
data_type_internal = sys.argv[10]
data_type_out = sys.argv[11]


ops = extractGraph(filename)
partitions = []
partitions_outputs = []

# find balanced partitions
curPartition = extractPartion(ops, out_ar, out_size, 0, 1)
opTime = schedule_he(curPartition, op_latency)
curLatency = getScheduleLen(opTime)
worstLatency = curLatency

# iterativaly increase the size of a partition until it exceed the target latency and,
# when it happens, reduce its size and include the leftover graph into the next partition
startOffset = 0
for i in range(2, out_size + 1):
	testPartition = extractPartion(ops, out_ar, out_size, startOffset, i)
	opTime = schedule_he(testPartition, op_latency)
	latency = getScheduleLen(opTime)

	if latency > target_lat:
		partitions.append(curPartition)
		worstLatency = max(curLatency, worstLatency)
		partitions_outputs.append(i - 1 - startOffset)
		startOffset = i - 1
		curPartition = extractPartion(ops, out_ar, out_size, startOffset, i)
		opTime = schedule_he(curPartition, op_latency)
		curLatency = getScheduleLen(opTime)
	else:
		curPartition = testPartition

partitions.append(curPartition)
partitions_outputs.append(out_size - startOffset)

# check that the target latency is met
if worstLatency > target_lat:
	print("failed to meet required latency: " + str(target_lat) + " achieved: " + str(latency))
	sys.exit(1)

# compute final schedule for each partition
schedules = []
for p in partitions:
	schedules.append(schedule_he(p, op_latency))

# generate the code for the load-store architecture
code = genCode(filename, schedules, partitions, partitions_outputs, in_ar, in_size, \
	out_ar, out_size, op_latency, custom_prefix, data_type_in, data_type_internal, data_type_out)

print(code)
