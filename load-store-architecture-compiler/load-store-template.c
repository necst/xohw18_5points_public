/*
 * authors:
 * Marco Rabozzi (marco.rabozzi@polimi.it)
 * Emanuele Del Sozzo (emanuele.delsozzo@polimi.it)
 * Lorenzo Di Tucci (lorenzo.ditucci@polimi.it)
 *
 * This code was auto generated using the load-store architecture compiler.
 * Original source code file: {original_filename}
 */
#include <stdint.h>
#include "hls_stream.h"

typedef {data_type_in} CFA_DATA_TYPE_IN;
typedef {data_type_internal} CFA_DATA_TYPE_INTERNAL;
typedef {data_type_out} CFA_DATA_TYPE_OUT;

{defines}

const uint32_t CFA_cores_instr[CFA_NUM_CORES][CFA_ISTR] = {
{instrs}
};

const uint32_t CFA_cores_num_outputs[CFA_NUM_CORES] = {
{num_outputs}
};

const uint32_t CFA_cores_output_offset[CFA_NUM_CORES] = {
{output_offsets}
};

void CFA_decode_instr(uint32_t instr, uint16_t &addr1, uint16_t &addr2, 
	uint16_t &addr3, uint8_t &op_code)
{
	op_code = instr >> 30;
	addr1 = (instr >> 20) & 0x3FF;
	addr2 = (instr >> 10) & 0x3FF;
	addr3 = instr & 0x3FF;
}

void CFA_core(CFA_DATA_TYPE_INTERNAL tmp1[CFA_NUM_REGS], CFA_DATA_TYPE_INTERNAL tmp2[CFA_NUM_REGS], const uint32_t instr[CFA_ISTR], const int num_outputs) 
{	
	// initialized used constants

{consts}

	for(int i = 0; i < CFA_ISTR; i++) {
		#pragma HLS PIPELINE
		#pragma HLS DEPENDENCE variable=tmp1 inter false
		#pragma HLS DEPENDENCE variable=tmp2 inter false
		uint16_t addr1, addr2, addr3;
		uint8_t op;
		CFA_decode_instr(instr[i], addr1, addr2, addr3, op);

		CFA_DATA_TYPE_INTERNAL v;
		switch(op) {
			case 0:
				v = tmp1[addr1]*tmp2[addr2];
				tmp1[addr3] = v;
				tmp2[addr3] = v;
				break;
			case 1:
				v = tmp1[addr1]+tmp2[addr2];
				tmp1[addr3] = v;
				tmp2[addr3] = v;
				break;
			case 2:
				v = tmp1[addr1]-tmp2[addr2];
				tmp1[addr3] = v;
				tmp2[addr3] = v;
				break;
		}
	}
}

void CFA_write_back(CFA_DATA_TYPE_INTERNAL tmp1[CFA_NUM_CORES][CFA_NUM_REGS], hls::stream<CFA_DATA_TYPE_OUT> &out)
{
	#pragma HLS INLINE off
	for(int j = 0; j < CFA_NUM_CORES; j++) {
		#pragma HLS UNROLL
		for(int i = 0; i < CFA_cores_num_outputs[j]; i++) {
			#pragma HLS UNROLL
			out.write(tmp1[j][CFA_ISTR + CFA_IN_SIZE + CFA_CONSTS + i]);
		}
	}
}

void dataRead(CFA_DATA_TYPE_INTERNAL tmp1[CFA_NUM_CORES][CFA_NUM_REGS], CFA_DATA_TYPE_INTERNAL tmp2[CFA_NUM_CORES][CFA_NUM_REGS], hls::stream<CFA_DATA_TYPE_IN>& in) 
{
	#pragma HLS INLINE off
	for (int i = 0; i < CFA_IN_SIZE; i++) {
		#pragma HLS PIPELINE
		double in_val = in.read();
		for (int j = 0; j < CFA_NUM_CORES; j++) {
			tmp1[j][i] = in_val;
			tmp2[j][i] = in_val;
		}
	}
}

void CFA_multiCore(hls::stream<CFA_DATA_TYPE_IN> &in, hls::stream<CFA_DATA_TYPE_OUT> &out)
{
	CFA_DATA_TYPE_INTERNAL tmp1[CFA_NUM_CORES][CFA_NUM_REGS];
	CFA_DATA_TYPE_INTERNAL tmp2[CFA_NUM_CORES][CFA_NUM_REGS];

	#pragma HLS ARRAY_PARTITION variable=CFA_cores_instr complete dim=1
	#pragma HLS ARRAY_PARTITION variable=CFA_cores_num_outputs complete dim=1
	#pragma HLS ARRAY_PARTITION variable=tmp1 complete dim=1
	#pragma HLS ARRAY_PARTITION variable=tmp2 complete dim=1

	#pragma HLS RESOURCE variable=tmp1 core=RAM_S2P_BRAM
	#pragma HLS RESOURCE variable=tmp2 core=RAM_S2P_BRAM

	dataRead(tmp1, tmp2, in);

	for(int j = 0; j < CFA_NUM_CORES; j++) {
		#pragma HLS UNROLL
		CFA_core(tmp1[j], tmp2[j], CFA_cores_instr[j], CFA_cores_num_outputs[j]);
	}

	CFA_write_back(tmp1, out);
}
