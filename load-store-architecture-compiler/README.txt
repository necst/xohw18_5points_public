This folder contains:
- coeff[A1|A2|C]_in.h
- core_coeffs_[A1|A2|C].h
- compile_for_hls_250_mhz.sh
- compile.py
- load-store-template.c


coeff[A1|A2|C]_in.h:
Files containing the the expressions to be compiled for the load-store architecture


core_coeffs_[A1|A2|C].h:
Expressions compiled for the load-store architecture. These are the files we used in Vivado HLS


compile_for_hls_250_mhz.sh:
Script to automatically compile the coeff*_in.h files into the core_coeffs_*.h files


compile.py:
load-store architecture compiler written in Python


load-store-template.c:
Template for the load-store architecture 
